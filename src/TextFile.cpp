//
// Copyright (c) Nathan Voglsam 2017
//

#include <fstream>
#include <iostream>
#include "TextFile.h"

namespace SCPU32
{
    TextFile::TextFile(const std::string& path)
    {
        Path = path;

        std::ifstream fileStream;
        fileStream.open(Path);

        if (!fileStream.good())
        {
            fileStream.close();
            std::cout << "failed to load file: " << Path << std::endl;
            return;
        }

        Text = std::string((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());

        fileStream.close();
    }

    TextFile::TextFile(const std::string& inText, const std::string& inPath)
    {
        Text = inText;
        Path = inPath;
    }
}
