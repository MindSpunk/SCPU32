//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_STRINGS_H
#define SCPU32_STRINGS_H

#include <string>

namespace SCPU32
{
    namespace Errors
    {

        const std::string INVALID_TYPEA     = " :<< Invalid instruction - TypeA instruction should have 4 parameters\n";  // NOLINT
        const std::string INVALID_TYPEB     = " :<< Invalid instruction - TypeB instruction should have 2 parameters\n";  // NOLINT
        const std::string INVALID_TYPEC     = " :<< Invalid instruction - TypeC instruction should have 1 parameter\n";   // NOLINT
        const std::string INVALID_TYPED     = " :<< Invalid instruction - TypeD instruction should have no parameters\n"; // NOLINT

        const std::string NON_INSTRUCTION   = " is not an instruction\n";                       // NOLINT
        const std::string NON_REGISTER      = " :<< Invalid instruction - No such register\n";  // NOLINT

        const std::string FAILED_TO_ENCODE  = " Instruction Failed to Encode\n"; //NOLINT

    }
}

#endif //SCPU32_STRINGS_H
