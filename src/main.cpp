#include "Processor.h"
#include "Assembler.h"

int main()
{

    SCPU32::Assembler assembler("test.asm");

    SCPU32::Processor p;
    p.Reset();
    p.SetProgram(assembler.Assemble());

    while(p.Cycle());

    return 0;
}