//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_CONSTANTS_H
#define SCPU32_CONSTANTS_H

#include <map>

namespace SCPU32
{
    namespace Info
    {
        static const int REGISTER_COUNT         = 16;
        static const int WORD_SIZE              = 4;
        static const int MEMORY_WORD_COUNT      = 0xFFFF;
        static const int MEMORY_MAX_ADDRESS     = (MEMORY_WORD_COUNT * WORD_SIZE) - 1;
        static const int MEMORY_LENGTH          =  MEMORY_WORD_COUNT * WORD_SIZE;

        static const int INSTR_OPPCODE_WIDTH    = 20;
        static const int INSTR_REGISTER_WIDTH   = 4;

        static const int OPPCODE_SHIFT          = 32 - INSTR_OPPCODE_WIDTH;
    }

    namespace Keywords
    {
        const char          DIRECTIVE_MARKER  = '.';
        const std::string   DATA_DIRECTIVE    = "data";
        const std::string   INT_DATA_NAME     = "int";
        const std::string   SHORT_DATA_NAME   = "short";
        const std::string   BYTE_DATA_NAME    = "byte";
        const std::string   FLOAT_DATA_NAME   = "float";
        const std::string   UINT_DATA_NAME    = "uint";
        const std::string   USHORT_DATA_NAME  = "ushort";
        const std::string   UBYTE_DATA_NAME   = "ubyte";
    }

    namespace Maps
    {
        const std::map<std::string, Register> RegisterMap
                {
                        {"a",  Register::A},
                        {"b",  Register::B},
                        {"c",  Register::C},
                        {"d",  Register::D},
                        {"e",  Register::E},
                        {"f",  Register::F},
                        {"g",  Register::G},
                        {"h",  Register::H},
                        {"i1", Register::I1},
                        {"i2", Register::I2},
                        {"ia", Register::IA},
                        {"bp", Register::BP},
                        {"sp", Register::SP},
                        {"ra", Register::RA},
                        {"pc", Register::PC}
                };

        const std::map<std::string, Instruction> InstructionMap
                {
                        {"add",   Instruction::ADD},
                        {"sub",   Instruction::SUB},
                        {"div",   Instruction::DIV},
                        {"mul",   Instruction::MUL},
                        {"mod",   Instruction::MOD},
                        {"divu",  Instruction::DIVU},
                        {"mulu",  Instruction::MULU},
                        {"modu",  Instruction::MODU},
                        {"addf",  Instruction::ADDF},
                        {"subf",  Instruction::SUBF},
                        {"divf",  Instruction::DIVF},
                        {"mulf",  Instruction::MULF},
                        {"lls",   Instruction::LLS},
                        {"lrs",   Instruction::LRS},
                        {"ars",   Instruction::ARS},
                        {"mov",   Instruction::MOV},
                        {"ldw",   Instruction::LDW},
                        {"stw",   Instruction::STW},
                        {"ldh",   Instruction::LDH},
                        {"sth",   Instruction::STH},
                        {"ldb",   Instruction::LDB},
                        {"stb",   Instruction::STB},
                        {"jmp",   Instruction::JMP},
                        {"jmr",   Instruction::JMR},
                        {"ieq",   Instruction::IEQ},
                        {"ine",   Instruction::INE},
                        {"igt",   Instruction::IGT},
                        {"ilt",   Instruction::ILT},
                        {"ige",   Instruction::IGE},
                        {"ile",   Instruction::ILE},
                        {"igtu",  Instruction::IGTU},
                        {"iltu",  Instruction::ILTU},
                        {"igeu",  Instruction::IGEU},
                        {"ileu",  Instruction::ILEU},
                        {"igtf",  Instruction::IGTF},
                        {"iltf",  Instruction::ILTF},
                        {"igef",  Instruction::IGEF},
                        {"ilef",  Instruction::ILEF},
                        {"or",    Instruction::OR},
                        {"and",   Instruction::AND},
                        {"xor",   Instruction::XOR},
                        {"hwn",   Instruction::HWN},
                        {"hws",   Instruction::HWS},
                        {"hwm",   Instruction::HWM},
                        {"hwv",   Instruction::HWV},
                        {"hwi",   Instruction::HWI},
                        {"int",   Instruction::INT},
                        {"nop",   Instruction::NOP},
                        {"pshw",  Instruction::PSHW},
                        {"pshh",  Instruction::PSHH},
                        {"pshb",  Instruction::PSHB},
                        {"popw",  Instruction::POPW},
                        {"poph",  Instruction::POPH},
                        {"popb",  Instruction::POPB},
                        {"pekw",  Instruction::PEKW},
                        {"pekh",  Instruction::PEKH},
                        {"pekb",  Instruction::PEKB},
                        {"csu",   Instruction::CSU},
                        {"cus",   Instruction::CUS},
                        {"cfs",   Instruction::CFS},
                        {"cfu",   Instruction::CFU},
                        {"csf",   Instruction::CSF},
                        {"cuf",   Instruction::CUF}
                };

        const std::map<Instruction, Type> TypeMap
                {
                        {Instruction::ADD, Type::TypeA},
                        {Instruction::SUB, Type::TypeA},
                        {Instruction::DIV, Type::TypeA},
                        {Instruction::MUL, Type::TypeA},
                        {Instruction::MOD, Type::TypeA},

                        {Instruction::DIVU, Type::TypeA},
                        {Instruction::MULU, Type::TypeA},
                        {Instruction::MODU, Type::TypeA},

                        {Instruction::ADDF, Type::TypeA},
                        {Instruction::SUBF, Type::TypeA},
                        {Instruction::DIVF, Type::TypeA},
                        {Instruction::MULF, Type::TypeA},

                        {Instruction::LLS, Type::TypeA},
                        {Instruction::LRS, Type::TypeA},
                        {Instruction::ARS, Type::TypeA},

                        {Instruction::MOV, Type::TypeB},

                        {Instruction::LDW, Type::TypeB},
                        {Instruction::STW, Type::TypeB},

                        {Instruction::LDH, Type::TypeB},
                        {Instruction::STH, Type::TypeB},

                        {Instruction::LDB, Type::TypeB},
                        {Instruction::STB, Type::TypeB},

                        {Instruction::JMP, Type::TypeC},
                        {Instruction::JMR, Type::TypeC},

                        {Instruction::IEQ, Type::TypeB},
                        {Instruction::INE, Type::TypeB},
                        {Instruction::IGT, Type::TypeB},
                        {Instruction::ILT, Type::TypeB},
                        {Instruction::IGE, Type::TypeB},
                        {Instruction::ILE, Type::TypeB},

                        {Instruction::IGTU, Type::TypeB},
                        {Instruction::ILTU, Type::TypeB},
                        {Instruction::IGEU, Type::TypeB},
                        {Instruction::ILEU, Type::TypeB},

                        {Instruction::IGTF, Type::TypeB},
                        {Instruction::ILTF, Type::TypeB},
                        {Instruction::IGEF, Type::TypeB},
                        {Instruction::ILEF, Type::TypeB},

                        {Instruction::OR,  Type::TypeA},
                        {Instruction::AND, Type::TypeA},
                        {Instruction::XOR, Type::TypeA},

                        {Instruction::HWN, Type::TypeC},
                        {Instruction::HWS, Type::TypeB},
                        {Instruction::HWM, Type::TypeB},
                        {Instruction::HWV, Type::TypeB},
                        {Instruction::HWI, Type::TypeC},

                        {Instruction::INT, Type::TypeD},
                        {Instruction::NOP, Type::TypeD},

                        {Instruction::PSHW, Type::TypeC},
                        {Instruction::PSHH, Type::TypeC},
                        {Instruction::PSHB, Type::TypeC},

                        {Instruction::POPW, Type::TypeC},
                        {Instruction::POPH, Type::TypeC},
                        {Instruction::POPB, Type::TypeC},

                        {Instruction::PEKW, Type::TypeC},
                        {Instruction::PEKH, Type::TypeC},
                        {Instruction::PEKB, Type::TypeC},

                        {Instruction::CSU, Type::TypeB},
                        {Instruction::CUS, Type::TypeB},

                        {Instruction::CFU, Type::TypeB},
                        {Instruction::CFS, Type::TypeB},

                        {Instruction::CSF, Type::TypeB},
                        {Instruction::CUF, Type::TypeB},
                };
    }
}

#endif //SCPU32_CONSTANTS_H
