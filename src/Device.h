//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_DEVICE_H
#define SCPU32_DEVICE_H


#include <cstdint>
#include <vector>
#include <string>

namespace SCPU32
{
    class Device
    {

    protected:

        std::string DeviceString;

    public:

        Device() = default;

    public:

        virtual uint32_t GetManufacturerID();
        virtual uint32_t GetVersionNumber();
        virtual uint32_t Interrupt();
        void GetDeviceStringArray(std::vector<uint8_t> array);
    };
}

#endif //SCPU32_DEVICE_H
