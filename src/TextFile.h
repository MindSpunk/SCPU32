//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef LIBRARYENGINE_TEXTFILEREADER_H
#define LIBRARYENGINE_TEXTFILEREADER_H

#include <string>

namespace SCPU32
{
    struct TextFile
    {

    public:

        std::string Text;
        std::string Path;

    public:

        TextFile() = default;
        TextFile(const std::string& inText, const std::string& inPath);
        explicit TextFile(const std::string& path);

    };
}

#endif //LIBRARYENGINE_TEXTFILEREADER_H
