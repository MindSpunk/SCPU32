//
// Created by Nathan Voglsam on 26/01/2018.
//

#include "Utils.h"
#include <algorithm>

void ToLowerCase(std::string &s)
{
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
}
