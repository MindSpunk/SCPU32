//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_PROGRAM_H
#define SCPU32_PROGRAM_H


#include <cstdint>
#include <vector>

struct Program
{
    std::vector<uint32_t> Instructions;
    std::vector<uint8_t> StaticData;
};


#endif //SCPU32_PROGRAM_H
