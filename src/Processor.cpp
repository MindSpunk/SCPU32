//
// Copyright (c) Nathan Voglsam 2017
//
#include "Processor.h"

namespace SCPU32
{
    bool Processor::Cycle()
    {
        if (Registers[(int)Register::PC] >= Instructions.size() || Running)
        {
            return false;
        }

        if (--Ticks > 0)
        {
            return true;
        }

        uint32_t instruction = Instructions[Registers[(int)Register::PC]];
        auto code = static_cast<Instruction>(instruction >> Info::OPPCODE_SHIFT);

        switch(code)
        {
            case Instruction::ADD:  ADD(instruction);  break;
            case Instruction::SUB:  SUB(instruction);  break;
            case Instruction::DIV:  DIV(instruction);  break;
            case Instruction::MUL:  MUL(instruction);  break;
            case Instruction::MOD:  MOD(instruction);  break;
            case Instruction::DIVU: DIVU(instruction); break;
            case Instruction::MULU: MULU(instruction); break;
            case Instruction::MODU: MODU(instruction); break;
            case Instruction::ADDF: ADDF(instruction); break;
            case Instruction::SUBF: SUBF(instruction); break;
            case Instruction::DIVF: DIVF(instruction); break;
            case Instruction::MULF: MULF(instruction); break;
            case Instruction::LLS:  LLS(instruction);  break;
            case Instruction::LRS:  LRS(instruction);  break;
            case Instruction::ARS:  ARS(instruction);  break;
            case Instruction::MOV:  MOV(instruction);  break;
            case Instruction::LDW:  LDW(instruction);  break;
            case Instruction::STW:  STW(instruction);  break;
            case Instruction::LDH:  LDH(instruction);  break;
            case Instruction::STH:  STH(instruction);  break;
            case Instruction::LDB:  LDB(instruction);  break;
            case Instruction::STB:  STB(instruction);  break;
            case Instruction::JMP:  JMP(instruction);  break;
            case Instruction::JMR:  JMR(instruction);  break;
            case Instruction::IEQ:  IEQ(instruction);  break;
            case Instruction::INE:  INE(instruction);  break;
            case Instruction::IGT:  IGT(instruction);  break;
            case Instruction::ILT:  ILT(instruction);  break;
            case Instruction::IGE:  IGE(instruction);  break;
            case Instruction::ILE:  ILE(instruction);  break;
            case Instruction::IGTU: IGTU(instruction); break;
            case Instruction::ILTU: ILTU(instruction); break;
            case Instruction::IGEU: IGEU(instruction); break;
            case Instruction::ILEU: ILEU(instruction); break;
            case Instruction::IGTF: IGTF(instruction); break;
            case Instruction::ILTF: ILTF(instruction); break;
            case Instruction::IGEF: IGEF(instruction); break;
            case Instruction::ILEF: ILEF(instruction); break;
            case Instruction::OR:   OR(instruction);   break;
            case Instruction::AND:  AND(instruction);  break;
            case Instruction::XOR:  XOR(instruction);  break;
            case Instruction::HWN:  HWN(instruction);  break;
            case Instruction::HWS:  HWS(instruction);  break;
            case Instruction::HWM:  HWM(instruction);  break;
            case Instruction::HWV:  HWV(instruction);  break;
            case Instruction::HWI:  HWI(instruction);  break;
            case Instruction::INT:  INT();             break;
            case Instruction::NOP:  NOP();             break;
            case Instruction::PSHW: PSHW(instruction); break;
            case Instruction::PSHH: PSHH(instruction); break;
            case Instruction::PSHB: PSHB(instruction); break;
            case Instruction::POPW: POPW(instruction); break;
            case Instruction::POPH: POPH(instruction); break;
            case Instruction::POPB: POPB(instruction); break;
            case Instruction::PEKW: PEKW(instruction); break;
            case Instruction::PEKH: PEKH(instruction); break;
            case Instruction::PEKB: PEKB(instruction); break;
            case Instruction::CSU:  CSU(instruction);  break;
            case Instruction::CUS:  CUS(instruction);  break;
            case Instruction::CFU:  CFU(instruction);  break;
            case Instruction::CFS:  CFS(instruction);  break;
            case Instruction::CSF:  CSF(instruction);  break;
            case Instruction::CUF:  CUF(instruction);  break;
        }

        if (Running)
        {
            return false;
        }

        return true;

    }

    void Processor::SetProgram(const Program& Code)
    {
        Instructions = std::vector<uint32_t>(Code.Instructions);

        Registers[(int)Register::BP] = (int)Code.StaticData.size();

        for (int i = 0; i < Code.StaticData.size(); ++i)
        {
            Memory[i] = Code.StaticData[i];
        }
    }

    void Processor::Reset()
    {

        for (uint32_t& i : Registers)
        {
            i = 0;
        }

        for (uint8_t& i : Memory)
        {
            i = 0;
        }

        Registers[(int)Register::SP] = (uint32_t) Info::MEMORY_MAX_ADDRESS;
        Registers[(int)Register::PC] = 0;
        Ticks = 1;
        Running = false;

    }

    void Processor::SetRunning(bool running)
    {
        Running = running;
    }

    void Processor::Exception(ExceptionCode code)
    {

    }

    void Processor::ExtractTypeA(uint32_t instruction, int &D, int &A, int &B)
    {
        D = ( instruction & 0x0000000F);
        B = ((instruction >> Info::INSTR_REGISTER_WIDTH) & 0x0000000F);
        A = ((instruction >> (Info::INSTR_REGISTER_WIDTH * 2)) & 0x0000000F);
    }

    void Processor::ExtractTypeB(uint32_t instruction, int &D, int &A)
    {
        D = ( instruction & 0x0000000F);
        A = ((instruction >> Info::INSTR_REGISTER_WIDTH) & 0x0000000F);
    }

    void Processor::ExtractTypeC(uint32_t instruction, int &A)
    {
        A = ( instruction & 0x0000000F);
    }

    template <typename T>
    T Processor::GetLiteral()
    {
        uint32_t value = Instructions[++Registers[(int)Register::PC]];
        return *reinterpret_cast<T*>(&value);
    }

    template <typename T>
    void Processor::ExtractValues(int A, int B, T &X, T &Y)
    {
        ExtractValue<T>(A, X);
        ExtractValue<T>(B, Y);
    }

    template<typename T>
    void Processor::ExtractValue(int A, T &X)
    {

        if (A == static_cast<int>(Register::LITERAL))
        {
            X = GetLiteral<T>();
        }
        else
        {
            X = *reinterpret_cast<T*>(&Registers[A]);
        }

    }

    void Processor::ADD(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        int32_t result = X + Y;

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 1;
    }

    void Processor::SUB(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        int32_t result = X - Y;

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 1;
    }

    void Processor::DIV(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        if (X == 0 || Y == 0)
        {
            Exception(ExceptionCode::DIVIDE_BY_ZERO);
            return;
        }

        int32_t result = X / Y;

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 3;
    }

    void Processor::MUL(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        int32_t result = X * Y;

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 2;
    }

    void Processor::MOD(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        int32_t result = X % Y;

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 3;
    }

    void Processor::DIVU(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X == 0 || Y == 0)
        {
            Exception(ExceptionCode::DIVIDE_BY_ZERO);
            return;
        }

        Registers[D] = X / Y;


        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::MULU(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        Registers[D] = X * Y;

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::MODU(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        Registers[D] = X % Y;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::ADDF(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        float result = (X - Y);

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::SUBF(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        float result = (X - Y);

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::DIVF(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        float result = (X - Y);

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 4;

    }

    void Processor::MULF(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        float result = (X - Y);

        Registers[D] = *reinterpret_cast<uint32_t*>(&result);

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::LLS(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        Registers[D] = X << Y;


        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::LRS(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        Registers[D] = X >> Y;


        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::ARS(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        Registers[D] = *reinterpret_cast<uint32_t*>(X >> Y);


        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::MOV(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        Registers[D] = X;

        Registers[(int)Register::PC] += 1;
        Ticks += 1;
    }

    void Processor::LDW(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        if (X >= Info::MEMORY_LENGTH)
        {
            Exception(ExceptionCode::INVALID_MEMORY_ADDRESS);
            return;
        }

        Word value{};

        value.Bytes[0] = Memory[X];
        value.Bytes[1] = Memory[X+1];
        value.Bytes[2] = Memory[X+2];
        value.Bytes[3] = Memory[X+3];

        Registers[D] = value.Word;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::STW(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        if (X >= Info::MEMORY_LENGTH)
        {
            Exception(ExceptionCode::INVALID_MEMORY_ADDRESS);
            return;
        }

        Word value{};
        value.Word = Registers[D];

        Memory[X]   = value.Bytes[0];
        Memory[X+1] = value.Bytes[1];
        Memory[X+2] = value.Bytes[2];
        Memory[X+3] = value.Bytes[3];


        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::LDH(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        if (X >= Info::MEMORY_LENGTH)
        {
            Exception(ExceptionCode::INVALID_MEMORY_ADDRESS);
            return;
        }

        Word value{};

        value.Bytes[2] = Memory[X];
        value.Bytes[3] = Memory[X+1];

        Registers[D] = value.Word;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::STH(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        if (X >= Info::MEMORY_LENGTH)
        {
            Exception(ExceptionCode::INVALID_MEMORY_ADDRESS);
            return;
        }

        Word value{};
        value.Word = Registers[D];

        Memory[X]   = value.Bytes[2];
        Memory[X+1] = value.Bytes[3];

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::LDB(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        if (X >= Info::MEMORY_LENGTH)
        {
            Exception(ExceptionCode::INVALID_MEMORY_ADDRESS);
            return;
        }

        Word value{};

        value.Bytes[3] = Memory[X];
        Registers[D] = value.Word;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::STB(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        if (X >= Info::MEMORY_LENGTH)
        {
            Exception(ExceptionCode::INVALID_MEMORY_ADDRESS);
            return;
        }

        Word value{};
        value.Word = Registers[D];

        Memory[X] = value.Bytes[3];

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::JMP(uint32_t instruction)
    {
        int A;
        ExtractTypeC(instruction, A);

        uint32_t T;
        ExtractValue<uint32_t>(A, T);

        if (T >= Instructions.size())
        {
            Exception(ExceptionCode::INVALID_PROGRAM_ADDRESS);
            return;
        }

        Registers[(int)Register::PC] = T;
        Ticks += 1;
    }

    void Processor::JMR(uint32_t instruction)
    {
        int A;
        ExtractTypeC(instruction, A);

        uint32_t T;
        ExtractValue<uint32_t>(A, T);

        if (T >= Instructions.size())
        {
            Exception(ExceptionCode::INVALID_PROGRAM_ADDRESS);
            return;
        }

        Registers[(int)Register::RA] = Registers[(int)Register::PC] + 1;
        Registers[(int)Register::PC] = T;
        Ticks += 2;
    }

    void Processor::IEQ(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X == Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;
    }

    void Processor::INE(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X != Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::IGT(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        if (X > Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::ILT(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        if (X < Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::IGE(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        if (X >= Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::ILE(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        int32_t X, Y;
        ExtractValues<int32_t>(A, B, X, Y);

        if (X <= Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::IGTU(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X > Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::ILTU(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X < Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::IGEU(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X >= Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::ILEU(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        if (X <= Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 2;

    }

    void Processor::IGTF(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        if (X > Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 4;

    }

    void Processor::ILTF(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        if (X < Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 4;

    }

    void Processor::IGEF(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        if (X >= Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 4;

    }

    void Processor::ILEF(uint32_t instruction)
    {

        int A, B;
        ExtractTypeB(instruction, A, B);

        float X, Y;
        ExtractValues<float>(A, B, X, Y);

        if (X <= Y);
        {
            Registers[(int)Register::PC] += 1;
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 4;

    }

    void Processor::OR(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        uint32_t result = X | Y;

        Registers[D] = result;

        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::AND(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        uint32_t result = X & Y;

        Registers[D] = result;

        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::XOR(uint32_t instruction)
    {

        int D, A, B;
        ExtractTypeA(instruction, D, A, B);

        uint32_t X, Y;
        ExtractValues<uint32_t>(A, B, X, Y);

        uint32_t result = X ^ Y;

        Registers[D] = result;

        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::HWN(uint32_t instruction)
    {

        int D;
        ExtractTypeC(instruction, D);

        Registers[D] = static_cast<uint32_t>(Devices.size());

        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::HWS(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X, Y;
        ExtractValues<uint32_t>(D, A, X, Y);

        std::vector<uint8_t> array;
        Devices[Y]->GetDeviceStringArray(array);

        for (size_t i = 0; i < 64; i++)
        {
            if (i >= array.size())
            {
                Memory[X + i] = 0;
            }
            else
            {
                Memory[X + i] = array[i];
            }
        }

        Registers[(int)Register::PC] += 1;
        Ticks += 12;

    }

    void Processor::HWM(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue(A, X);

        Registers[D] = Devices[X]->GetManufacturerID();

        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::HWV(uint32_t instruction)
    {

        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue(A, X);

        Registers[D] = Devices[X]->GetVersionNumber();

        Registers[(int)Register::PC] += 1;
        Ticks += 1;

    }

    void Processor::HWI(uint32_t instruction)
    {

        int D;
        ExtractTypeC(instruction, D);

        uint32_t X;
        ExtractValue(D, X);

        int ticks = Devices[X]->Interrupt();

        Registers[(int)Register::PC] += 1;
        Ticks += 1 + ticks;

    }

    void Processor::INT()
    {
        if (Registers[(int)Register::IA] != 0)
        {
            Registers[(int)Register::PC] = Registers[(int)Register::IA];
        }
        Ticks += 1;
    }

    void Processor::NOP()
    {
        Registers[(int)Register::PC] += 1;
        Ticks += 1;
    }

    void Processor::PSHW(uint32_t instruction)
    {
        int A;
        ExtractTypeC(instruction, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        Word V{};
        V.Word = X;

        Registers[(int)Register::SP] -= 4;
        uint32_t BaseAddress = Registers[(int)Register::SP];

        Memory[BaseAddress]   = V.Bytes[0];
        Memory[BaseAddress+1] = V.Bytes[1];
        Memory[BaseAddress+2] = V.Bytes[2];
        Memory[BaseAddress+3] = V.Bytes[3];

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::PSHH(uint32_t instruction)
    {

        int A;
        ExtractTypeC(instruction, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        Word V{};
        V.Word = X;

        Registers[(int)Register::SP] -= 2;
        uint32_t BaseAddress = Registers[(int)Register::SP];

        Memory[BaseAddress]   = V.Bytes[2];
        Memory[BaseAddress+1] = V.Bytes[3];

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::PSHB(uint32_t instruction)
    {

        int A;
        ExtractTypeC(instruction, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        Word V{};
        V.Word = X;

        Registers[(int)Register::SP] -= 1;
        uint32_t BaseAddress = Registers[(int)Register::SP];

        Memory[BaseAddress]   = V.Bytes[3];

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::POPW(uint32_t instruction)
    {

        int A;
        ExtractTypeC(instruction, A);

        uint32_t BaseAddress = Registers[(int)Register::SP];

        Word V{};

        V.Bytes[0] = Memory[BaseAddress];
        V.Bytes[1] = Memory[BaseAddress+1];
        V.Bytes[2] = Memory[BaseAddress+2];
        V.Bytes[3] = Memory[BaseAddress+3];

        Registers[A] = V.Word;

        Registers[(int)Register::SP] += 4;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::POPH(uint32_t instruction)
    {

        int A;
        ExtractTypeC(instruction, A);

        uint32_t BaseAddress = Registers[(int)Register::SP];

        Word V{};

        V.Bytes[2] = Memory[BaseAddress];
        V.Bytes[3] = Memory[BaseAddress+1];

        Registers[A] = V.Word;

        Registers[(int)Register::SP] += 2;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::POPB(uint32_t instruction)
    {

        int A;
        ExtractTypeC(instruction, A);

        uint32_t BaseAddress = Registers[(int)Register::SP];

        Word V{};

        V.Bytes[3] = Memory[BaseAddress];

        Registers[A] = V.Word;

        Registers[(int)Register::SP] += 1;

        Registers[(int)Register::PC] += 1;
        Ticks += 3;

    }

    void Processor::PEKW(uint32_t instruction)
    {
        POPW(instruction);
        Registers[(int)Register::SP] -= 4;
        Ticks += 1;
    }

    void Processor::PEKH(uint32_t instruction)
    {
        POPH(instruction);
        Registers[(int)Register::SP] -= 2;
        Ticks += 1;
    }

    void Processor::PEKB(uint32_t instruction)
    {
        POPB(instruction);
        Registers[(int)Register::SP] -= 2;
        Ticks += 1;
    }

    void Processor::CSU(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        int32_t X;
        ExtractValue<int32_t>(A, X);

        Registers[D] = static_cast<uint32_t>(X);

        Registers[(int)Register::PC] += 1;
        Ticks += 3;
    }

    void Processor::CUS(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        auto VAL = static_cast<int32_t>(X);

        Registers[D] = *reinterpret_cast<uint32_t*>(&VAL);

        Registers[(int)Register::PC] += 1;
        Ticks += 3;
    }

    void Processor::CFU(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        float X;
        ExtractValue<float>(A, X);

        Registers[D] = static_cast<uint32_t>(X);

        Registers[(int)Register::PC] += 1;
        Ticks += 5;
    }

    void Processor::CFS(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        float X;
        ExtractValue<float>(A, X);

        auto VAL = static_cast<int32_t>(X);

        Registers[D] = *reinterpret_cast<uint32_t*>(&VAL);

        Registers[(int)Register::PC] += 1;
        Ticks += 5;
    }

    void Processor::CSF(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        int32_t X;
        ExtractValue<int32_t>(A, X);

        auto VAL = static_cast<float>(X);

        Registers[D] = *reinterpret_cast<uint32_t*>(&VAL);

        Registers[(int)Register::PC] += 1;
        Ticks += 5;
    }

    void Processor::CUF(uint32_t instruction)
    {
        int D, A;
        ExtractTypeB(instruction, D, A);

        uint32_t X;
        ExtractValue<uint32_t>(A, X);

        auto VAL = static_cast<float>(X);

        Registers[D] = *reinterpret_cast<uint32_t*>(&VAL);

        Registers[(int)Register::PC] += 1;
        Ticks += 5;
    }

}