//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_ASSEMBLER_H
#define SCPU32_ASSEMBLER_H

#include <string>
#include <vector>
#include <map>
#include "Enums.h"
#include "Program.h"

namespace SCPU32
{
    class Assembler
    {

    protected:

        std::string Text;
        std::map<std::string, size_t> DataAddresses;

    public:

        explicit Assembler(const std::string& path);

    public:

        Program Assemble();

    protected:

        void SplitToLines(const std::string& text, std::vector<std::string>& lines);
        void SplitToWords(const std::string& text, std::vector<std::string>& words);

        bool EncodeTypeA(Instruction i, std::vector<uint32_t>& code, const std::string& line);
        bool EncodeTypeB(Instruction i, std::vector<uint32_t>& code, const std::string& line);
        bool EncodeTypeC(Instruction i, std::vector<uint32_t>& code, const std::string& line);
        bool EncodeTypeD(Instruction i, std::vector<uint32_t>& code, const std::string& line);

    };
}

#endif //SCPU32_ASSEMBLER_H
