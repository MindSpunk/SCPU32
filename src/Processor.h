//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_PROCESSOR_H
#define SCPU32_PROCESSOR_H


#include <cstdint>
#include <vector>
#include "Enums.h"
#include "Device.h"
#include "Constants.h"
#include "Program.h"
#include "Utils.h"

namespace SCPU32
{
    class Processor
    {

    protected:

        std::vector<uint32_t>   Instructions;
        std::vector<Device*>    Devices;

        uint32_t                Registers[Info::REGISTER_COUNT]   {};
        uint8_t                 Memory[Info::MEMORY_LENGTH]       {};
        uint32_t                Ticks                       = 1;

        bool                    Running                     = false;

    public:

        Processor() = default;

    public:

        void SetProgram(const Program& Code);
        bool Cycle();
        void Reset();
        void SetRunning(bool running);
        void Exception(ExceptionCode code);

    protected:

        void ExtractTypeA(uint32_t instruction, int& D, int& A, int& B);
        void ExtractTypeB(uint32_t instruction, int& D, int& A);
        void ExtractTypeC(uint32_t instruction, int& A);

        template <typename T> T GetLiteral();
        template <typename T> void ExtractValues(int A, int B, T &X, T &Y);
        template <typename T> void ExtractValue(int A, T &X);

        void ADD(uint32_t instruction);
        void SUB(uint32_t instruction);
        void DIV(uint32_t instruction);
        void MUL(uint32_t instruction);
        void MOD(uint32_t instruction);

        void DIVU(uint32_t instruction);
        void MULU(uint32_t instruction);
        void MODU(uint32_t instruction);

        void ADDF(uint32_t instruction);
        void SUBF(uint32_t instruction);
        void DIVF(uint32_t instruction);
        void MULF(uint32_t instruction);

        void LLS(uint32_t instruction);
        void LRS(uint32_t instruction);

        void ARS(uint32_t instruction);

        void MOV(uint32_t instruction);

        void LDW(uint32_t instruction);
        void STW(uint32_t instruction);

        void LDH(uint32_t instruction);
        void STH(uint32_t instruction);

        void LDB(uint32_t instruction);
        void STB(uint32_t instruction);

        void JMP(uint32_t instruction);
        void JMR(uint32_t instruction);

        void IEQ(uint32_t instruction);
        void INE(uint32_t instruction);
        void IGT(uint32_t instruction);
        void ILT(uint32_t instruction);
        void IGE(uint32_t instruction);
        void ILE(uint32_t instruction);

        void IGTU(uint32_t instruction);
        void ILTU(uint32_t instruction);
        void IGEU(uint32_t instruction);
        void ILEU(uint32_t instruction);

        void IGTF(uint32_t instruction);
        void ILTF(uint32_t instruction);
        void IGEF(uint32_t instruction);
        void ILEF(uint32_t instruction);

        void OR(uint32_t instruction);
        void AND(uint32_t instruction);
        void XOR(uint32_t instruction);

        void HWN(uint32_t instruction);
        void HWS(uint32_t instruction);
        void HWM(uint32_t instruction);
        void HWV(uint32_t instruction);
        void HWI(uint32_t instruction);

        void INT();

        void NOP();

        void PSHW(uint32_t instruction);
        void PSHH(uint32_t instruction);
        void PSHB(uint32_t instruction);

        void POPW(uint32_t instruction);
        void POPH(uint32_t instruction);
        void POPB(uint32_t instruction);

        void PEKW(uint32_t instruction);
        void PEKH(uint32_t instruction);
        void PEKB(uint32_t instruction);

        void CSU(uint32_t instruction);
        void CUS(uint32_t instruction);

        void CFU(uint32_t instruction);
        void CFS(uint32_t instruction);

        void CSF(uint32_t instruction);
        void CUF(uint32_t instruction);
    };
}


#endif //SCPU32_PROCESSOR_H
