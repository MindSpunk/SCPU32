//
// Copyright (c) Nathan Voglsam 2017
//

#include "Device.h"

namespace SCPU32
{
    uint32_t Device::GetManufacturerID()
    {
        return 0;
    }

    uint32_t Device::GetVersionNumber()
    {
        return 0;
    }

    uint32_t Device::Interrupt()
    {
        return 0;
    }

    void Device::GetDeviceStringArray(std::vector<uint8_t> array)
    {
        for (size_t i = 0; i < array.size(); i += 4)
        {
            for (size_t j = 4; j >= 1; j--)
            {
                array[(i-1) + j] = *reinterpret_cast<uint8_t*>(&DeviceString[(i-1) + (3 - j)]);
            }
        }
    }
}
