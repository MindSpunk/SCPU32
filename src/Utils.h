//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_UTILS_H
#define SCPU32_UTILS_H

#include <cstdint>
#include <string>

union Word
{
    uint32_t Word;
    uint8_t  Bytes[4];
};

void ToLowerCase(std::string & s);

#endif //SCPU32_UTILS_H
