//
// Copyright (c) Nathan Voglsam 2017
//

#include <sstream>
#include <iostream>
#include "Assembler.h"
#include "TextFile.h"
#include "Constants.h"
#include "Strings.h"
#include "Utils.h"

namespace SCPU32
{
    Assembler::Assembler(const std::string &path)
    {

        TextFile file(path);
        Text = file.Text;

    }

    Program Assembler::Assemble()
    {

        std::vector<uint32_t> code;
        std::vector<uint8_t> data;
        std::vector<std::string> lines;
        SplitToLines(Text, lines);

        for (auto& line : lines)
        {

            Instruction I;
            std::vector<std::string> words;
            SplitToWords(line, words);

            for (auto& word : words)
            {
                ToLowerCase(word);
            }

            if (words[0].at(0) == Keywords::DIRECTIVE_MARKER)
            {
                if (words[0].substr(1, words[0].size()-1) == Keywords::DATA_DIRECTIVE)
                {
                    // TODO: MAKE THIS LESS UGLY
                    // INT ===========================================
                    if (words[1] == Keywords::INT_DATA_NAME)
                    {
                        int32_t value = static_cast<int32_t>(std::stol(words[3]));
                        Word word{};
                        word.Word = *reinterpret_cast<uint32_t*>(&value);

                        DataAddresses[words[2]] = data.size();
                        data.emplace_back(word.Bytes[0]);
                        data.emplace_back(word.Bytes[1]);
                        data.emplace_back(word.Bytes[2]);
                        data.emplace_back(word.Bytes[3]);
                        continue;
                    }

                    if (words[1] == Keywords::UINT_DATA_NAME)
                    {
                        uint32_t value = static_cast<uint32_t>(std::stoul(words[3]));
                        Word word{};
                        word.Word = value;

                        DataAddresses[words[2]] = data.size();
                        data.emplace_back(word.Bytes[0]);
                        data.emplace_back(word.Bytes[1]);
                        data.emplace_back(word.Bytes[2]);
                        data.emplace_back(word.Bytes[3]);
                        continue;
                    }
                    // SHORT ========================================
                    if (words[1] == Keywords::SHORT_DATA_NAME)
                    {
                        int16_t value = static_cast<int16_t>(std::stol(words[3]));
                        Word word{};
                        word.Word = *reinterpret_cast<uint32_t*>(&value);

                        DataAddresses[words[2]] = data.size();
                        data.emplace_back(word.Bytes[1]);
                        data.emplace_back(word.Bytes[2]);
                        continue;
                    }

                    if (words[1] == Keywords::USHORT_DATA_NAME)
                    {
                        uint16_t value = static_cast<uint16_t>(std::stoul(words[3]));
                        Word word{};
                        word.Word = *reinterpret_cast<uint32_t*>(&value);

                        DataAddresses[words[2]] = data.size();
                        data.emplace_back(word.Bytes[1]);
                        data.emplace_back(word.Bytes[2]);
                        continue;
                    }
                    // BYTE =========================================
                    if (words[1] == Keywords::BYTE_DATA_NAME)
                    {
                        int8_t value = static_cast<int8_t>(std::stol(words[3]));
                        Word word{};
                        word.Word = *reinterpret_cast<uint8_t*>(&value);

                        DataAddresses[words[2]] = data.size();
                        data.emplace_back(word.Bytes[0]);
                        continue;
                    }

                    if (words[1] == Keywords::UBYTE_DATA_NAME)
                    {
                        uint8_t value = static_cast<uint8_t>(std::stoul(words[3]));
                        Word word{};
                        word.Word = *reinterpret_cast<uint32_t*>(&value);

                        DataAddresses[words[2]] = data.size();
                        data.emplace_back(word.Bytes[0]);
                        continue;
                    }
                }
            }
            else
            {
                try
                {
                    I = Maps::InstructionMap.at(words[0]);
                }
                catch (const std::out_of_range& e)
                {
                    std::cout << words[0] << Errors::NON_INSTRUCTION;
                    return Program();
                }
            }


            bool success;
            switch (Maps::TypeMap.at(I))
            {
                case Type::TypeA:
                {
                    success = EncodeTypeA(I, code, line);
                    break;
                }
                case Type::TypeB:
                {
                    success = EncodeTypeB(I, code, line);
                    break;
                }
                case Type::TypeC:
                {
                    success = EncodeTypeC(I, code, line);
                    break;
                }
                case Type::TypeD:
                {
                    success = EncodeTypeD(I, code, line);
                    break;
                }
            }
            if (!success)
            {
                std::cout << Errors::FAILED_TO_ENCODE;
                return Program();
            }
        }

        for (uint32_t& s : code)
        {
            std::stringstream stream;
            stream << std::hex << s;
            std::cout << stream.str() << '\n';
        }


        Program program;
        program.Instructions = code;
        program.StaticData = data;

        return program;
    }

    bool Assembler::EncodeTypeA(Instruction i, std::vector<uint32_t> &code, const std::string &line)
    {
        std::vector<std::string> words;
        SplitToWords(line, words);

        if (words.size() != 4)
        {
            std::cout << line << Errors::INVALID_TYPEA;
            return false;
        }

        for (auto& word : words)
        {
            ToLowerCase(word);
        }

        auto oppcode = static_cast<uint32_t>(i);
        oppcode = oppcode << Info::OPPCODE_SHIFT;

        Register p0;
        Register p1;
        Register p2;

        try
        {
            p0 = Maps::RegisterMap.at(words[1]);
            p1 = Maps::RegisterMap.at(words[2]);
            p2 = Maps::RegisterMap.at(words[3]);
        }
        catch (const std::out_of_range& e)
        {
            // TODO: Implement reading of labels
            // TODO: Implement reading of literals
            std::cout << line << Errors::NON_REGISTER;
            return false;
        }

        uint32_t B = (static_cast<uint32_t>(p2) << 2*Info::INSTR_REGISTER_WIDTH) & 0x00000F00;
        uint32_t A = (static_cast<uint32_t>(p1) << Info::INSTR_REGISTER_WIDTH) & 0x000000F0;
        uint32_t D = (static_cast<uint32_t>(p0)) & 0x0000000F;
        oppcode = oppcode | D | A | B;

        code.emplace_back(oppcode);
        return true;
    }

    bool Assembler::EncodeTypeB(Instruction i, std::vector<uint32_t> &code, const std::string &line)
    {
        std::vector<std::string> words;
        SplitToWords(line, words);

        if (words.size() != 3)
        {
            std::cout << line << Errors::INVALID_TYPEB;
            return false;
        }

        for (auto& word : words)
        {
            ToLowerCase(word);
        }

        auto oppcode = static_cast<uint32_t>(i);
        oppcode = oppcode << Info::OPPCODE_SHIFT;

        Register p0;
        Register p1;

        try
        {
            p0 = Maps::RegisterMap.at(words[1]);
            p1 = Maps::RegisterMap.at(words[2]);
        }
        catch (const std::out_of_range& e)
        {
            p1 = Register::LITERAL;
            // TODO: Implement reading of labels
            // TODO: Implement reading of literals CURRENTLY USING HACK FOR A TEST
            std::cout << line << Errors::NON_REGISTER;
            //return false;
        }

        uint32_t D = (static_cast<uint32_t>(p0)) & 0x0000000F;
        uint32_t A = (static_cast<uint32_t>(p1) << Info::INSTR_REGISTER_WIDTH) & 0x000000F0;
        oppcode = (oppcode | D) | A;

        code.emplace_back(oppcode);

        if (p1 == Register::LITERAL)
        {
            int val = std::stoi(words[2]);
            code.emplace_back(*reinterpret_cast<uint32_t*>(&val));
        }

        return true;
    }

    bool Assembler::EncodeTypeC(Instruction i, std::vector<uint32_t> &code, const std::string &line)
    {
        std::vector<std::string> words;
        SplitToWords(line, words);

        if (words.size() != 2)
        {
            std::cout << line << Errors::INVALID_TYPEC;
            return false;
        }

        for (auto& word : words)
        {
            ToLowerCase(word);
        }

        auto oppcode = static_cast<uint32_t>(i);
        oppcode = oppcode << Info::OPPCODE_SHIFT;

        Register p0;

        try
        {
            p0 = Maps::RegisterMap.at(words[1]);
        }
        catch (const std::out_of_range& e)
        {
            // TODO: Implement reading of labels
            // TODO: Implement reading of literals
            std::cout << line << Errors::NON_REGISTER;
            return false;
        }

        uint32_t D = static_cast<uint32_t>(p0) & 0x0000000F;
        oppcode = oppcode | D;

        code.emplace_back(oppcode);
        return true;
    }

    bool Assembler::EncodeTypeD(Instruction i, std::vector<uint32_t>& code, const std::string& line)
    {
        std::vector<std::string> words;
        SplitToWords(line, words);

        if (words.size() != 1)
        {
            std::cout << line << Errors::INVALID_TYPED;
            return false;
        }

        for (auto& word : words)
        {
            ToLowerCase(word);
        }

        auto oppcode = static_cast<uint32_t>(i);
        oppcode = oppcode << Info::OPPCODE_SHIFT;

        code.emplace_back(oppcode);
        return true;
    }

    void Assembler::SplitToLines(const std::string& text, std::vector<std::string>& lines)
    {
        std::stringstream stream(text);
        std::string token;

        while(std::getline(stream, token, '\n'))
        {
            if (token[0] != '#' && !token.empty())
            {
                lines.emplace_back(token);
            }
        }
    }

    void Assembler::SplitToWords(const std::string &text, std::vector<std::string> &words)
    {
        std::stringstream stream(text);
        std::string token;

        while(std::getline(stream, token, ' '))
        {
            if (token[token.size()-1] == ',')
            {
                token.erase(token.size()-1, 1);
            }
            if (!token.empty())
            {
                words.emplace_back(token);
            }
        }
    }
}
