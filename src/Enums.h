//
// Copyright (c) Nathan Voglsam 2017
//

#ifndef SCPU32_ENUMS_H
#define SCPU32_ENUMS_H

namespace SCPU32
{
    enum class Type : uint8_t
    {
        TypeA,
        TypeB,
        TypeC,
        TypeD
    };

    enum class Register : uint8_t
    {
        A       = 0x0,
        B       = 0x1,
        C       = 0x2,
        D       = 0x3,
        E       = 0x4,
        F       = 0x5,
        G       = 0x6,
        H       = 0x7,
        I1      = 0x8,
        I2      = 0x9,
        IA      = 0xA,
        BP      = 0xB,
        SP      = 0xC,
        RA      = 0xD,
        PC      = 0xE,
        LITERAL = 0xF,
    };

    enum class ExceptionCode : uint8_t
    {
        DIVIDE_BY_ZERO          = 0x01,
        INVALID_MEMORY_ADDRESS  = 0x02,
        INVALID_PROGRAM_ADDRESS = 0x03,
    };

    enum class Instruction : uint8_t
    {
        ADD     = 0x00000,
        SUB     = 0x00001,
        DIV     = 0x00002,
        MUL     = 0x00003,
        MOD     = 0x00004,

        DIVU    = 0x00005,
        MULU    = 0x00006,
        MODU    = 0x00007,

        ADDF    = 0x00008,
        SUBF    = 0x00009,
        DIVF    = 0x0000A,
        MULF    = 0x0000B,

        LLS     = 0x0000C,
        LRS     = 0x0000D,
        ARS     = 0x0000E,

        MOV     = 0x0000F,

        LDW     = 0x00010,
        STW     = 0x00011,

        LDH     = 0x00012,
        STH     = 0x00013,

        LDB     = 0x00014,
        STB     = 0x00015,

        JMP     = 0x00016,
        JMR     = 0x00017,

        IEQ     = 0x00018,
        INE     = 0x00019,
        IGT     = 0x0001A,
        ILT     = 0x0001B,
        IGE     = 0x0001C,
        ILE     = 0x0001D,

        IGTU    = 0x0001E,
        ILTU    = 0x0001F,
        IGEU    = 0x00020,
        ILEU    = 0x00021,

        IGTF    = 0x00022,
        ILTF    = 0x00023,
        IGEF    = 0x00024,
        ILEF    = 0x00025,

        OR      = 0x00026,
        AND     = 0x00027,
        XOR     = 0x00028,

        HWN     = 0x00029,
        HWS     = 0x0002A,
        HWM     = 0x0002B,
        HWV     = 0x0002C,
        HWI     = 0x0002D,

        INT     = 0x0002E,

        NOP     = 0x0002F,

        PSHW    = 0x00030,
        PSHH    = 0x00031,
        PSHB    = 0x00032,

        POPW    = 0x00033,
        POPH    = 0x00034,
        POPB    = 0x00035,

        PEKW    = 0x00036,
        PEKH    = 0x00037,
        PEKB    = 0x00038,

        CSU     = 0x00039,
        CUS     = 0x0003A,

        CFU     = 0x0003B,
        CFS     = 0x0003C,

        CSF     = 0x0003D,
        CUF     = 0x0003E,
    };
}

#endif //SCPU32_ENUMS_H
